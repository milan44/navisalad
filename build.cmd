@echo off

rmdir "Navisalad" /s /q

nativefier --name "Navisalad" --arch "x64" --app-copyright "Milan Bartky" --icon "./navisalad.ico" --maximize --disable-context-menu --disable-dev-tools --single-instance --clear-cache "https://navaids.saladair.org"