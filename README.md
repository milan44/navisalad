## Installation

1. Download the latest version [here](https://gitlab.com/milan44/navisalad/raw/master/Navisalad.zip)
2. Extract the "Navisalad" folder inside the zip file
3. Run the "Navisalad.exe" inside that folder